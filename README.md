## Overview

This is a simple script I wrote for myself to help me keep an inventory of my
Raspberry Pi collection.

It uses info from `/proc/cpuinfo`, `vcgencmd`, `/etc/os_release`, `uname` and
a lookup table from `http://elinux.org/RPi_HardwareHistory` (which has to be
supplied by the user).

## Usage

Just run it on a Raspberry Pi (tested on `Raspbian` and `Raspberry Pi OS`). So
far I have tested it on an original model B, a B+, a Pi 2, a Pi 3 and a Pi 3B+
and the Pi 4B 4GB and 8GB. It's also been used on all versions of the Zero to
date.

```
$ ./what_pi
Revision     : 0010
Release date : Q3 2014
Model        : B+
PCB Revision : 1.0
Memory       : 512 MB
Notes        : (Mfg by Sony)
Serial no    : 00000000123a456d

Operating system details:
Release      : Raspbian GNU/Linux 9 (stretch)
Kernel       : 4.19.66+ #1253 Thu Aug 15 11:37:30 BST 2019
Machine      : armv6l

Various configuration and other settings:
CPU temp=36.9'C
H264:   H264=enabled
MPG2:   MPG2=disabled
WVC1:   WVC1=disabled
MPG4:   MPG4=enabled
MJPG:   MJPG=enabled
WMV9:   WMV9=disabled
sdtv_mode=0
sdtv_aspect=0

Network information:
  Hostname   : rpi2
  MAC        : b8:27:eb:22:9a:8c (eth0)
    inet  192.168.0.65  netmask 255.255.255.0  broadcast 192.168.0.255
    inet6 fe80::8131:8c1:cd12:304b  prefixlen 64  scopeid 0x20<link>
```

## Update

Now added to the group `oss_tools`

- Originally: `git@gitlab.com:davmo/what_pi.git`
- Now: `git@gitlab.com:oss_tools/what_pi.git`

<!--
vim: syntax=markdown:ts=8:sw=4:ai:et:tw=78:fo=tcqn:fdm=marker
-->

